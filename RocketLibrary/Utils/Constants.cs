﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Utils
{
    public static class Constants
    {
        public const int LandingAreaDefaultWidth = 100;
        public const int LandingAreaDefaultHeigth = 100;
        public const int LandingPlatformDefaultWidth = 10;
        public const int LandingPlatformDefaultHeigth = 10;
        public const int LandingPlatformDefaultX = 5;
        public const int LandingPlatformDefaultY = 5;
    }
}
