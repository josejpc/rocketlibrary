﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Model
{
    public enum LandingResult
    {
        OutOfPlatform,
        Clash,
        OkForLanding
    }
}
