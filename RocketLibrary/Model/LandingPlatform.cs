﻿using RocketLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Model
{
    public class LandingPlatform
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public Point StartingPoint { get; set; }        
        public LandingPlatform()
        {
            Width = Constants.LandingPlatformDefaultWidth;
            Height = Constants.LandingPlatformDefaultHeigth;
            StartingPoint = new Point
            {
                X = Constants.LandingPlatformDefaultX,
                Y = Constants.LandingPlatformDefaultY
            };
        }
    }
}
