﻿using RocketLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Model
{
    public class LandingArea
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public LandingPlatform LandingPlatform;

        public LandingArea()
        {
            Width = Constants.LandingAreaDefaultWidth;
            Height = Constants.LandingAreaDefaultWidth;
            LandingPlatform = new LandingPlatform();
        }
    }    
}
