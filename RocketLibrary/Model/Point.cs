﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Model
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
