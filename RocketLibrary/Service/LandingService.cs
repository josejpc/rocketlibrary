﻿using RocketLibrary.Model;

namespace RocketLibrary.Service
{
    public class LandingService : ILandingService
    {
        private LandingArea LandingArea;
        private Point LastLandingPoint;

        public LandingService()
        {
            LandingArea = new LandingArea();
        }

        /// <summary>
        /// Checks if a point is correct for landing
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <returns><see cref="LandingResult"/> with the landing result</returns>
        public LandingResult CheckLanding(int x, int y)
        {
            LandingResult result = LandingResult.OutOfPlatform;

            //If x and y coordinates are equal, +1 or -1 compared to the last checked point, then it's a clash
            if (LastLandingPoint != null && LastLandingPoint.X >= x - 1 && LastLandingPoint.X <= x + 1 && LastLandingPoint.Y >= y - 1 && LastLandingPoint.Y <= y + 1)
            {
                result = LandingResult.Clash;
            }
            //Now check if the position is inside the landing platform
            else if (x >= LandingArea.LandingPlatform.StartingPoint.X && x <= LandingArea.LandingPlatform.StartingPoint.X + LandingArea.LandingPlatform.Width
                && y >= LandingArea.LandingPlatform.StartingPoint.Y && y <= LandingArea.LandingPlatform.StartingPoint.Y + LandingArea.LandingPlatform.Height)
            {
                result = LandingResult.OkForLanding;
            }

            //If it's not a clash and it's not inside the platform, then it's outside the platform (default value)            
            LastLandingPoint = new Point
            {
                X = x,
                Y = y
            };

            return result;
        }

        /// <summary>
        /// Set landing platform width and height
        /// </summary>
        /// <param name="width">The width</param>
        /// <param name="heigth">The height</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        public bool SetPlatformSize(int width, int heigth)
        {
            //The landing platform needs to be inside the landing area
            if (width > 0 && heigth > 0 && LandingArea.Width >= LandingArea.LandingPlatform.StartingPoint.X + width
                && LandingArea.Height >= LandingArea.LandingPlatform.StartingPoint.Y + heigth)
            {
                LandingArea.LandingPlatform.Width = width;
                LandingArea.LandingPlatform.Height = heigth;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Set landing platform starting point
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        public bool SetPlatformStartingPoint(int x, int y)
        {
            //The landing platform needs to be inside the landing area
            if (x > 0 && y > 0 && LandingArea.Width >= LandingArea.LandingPlatform.Width + x
                && LandingArea.Height >= LandingArea.LandingPlatform.Height + y)
            {
                LandingArea.LandingPlatform.StartingPoint.X = x;
                LandingArea.LandingPlatform.StartingPoint.Y = y;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Set landing area width and height
        /// </summary>
        /// <param name="width">The width</param>
        /// <param name="heigth">The height</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        public bool SetLandingAreaSize(int width, int heigth)
        {
            //The landing area needs to contain the landing platform
            if (width > 0 && heigth > 0 && width >= LandingArea.LandingPlatform.Width + LandingArea.LandingPlatform.StartingPoint.X
                && heigth >= LandingArea.LandingPlatform.Height + LandingArea.LandingPlatform.StartingPoint.Y)
            {
                LandingArea.Width = width;
                LandingArea.Height = heigth;
                return true;
            }

            return false;
        }
    }
}
