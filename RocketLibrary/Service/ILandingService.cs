﻿using RocketLibrary.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RocketLibrary.Service
{
    public interface ILandingService
    {
        /// <summary>
        /// Checks if a point is correct for landing
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <returns><see cref="LandingResult"/> with the landing result</returns>
        LandingResult CheckLanding(int x, int y);

        /// <summary>
        /// Set landing platform width and height
        /// </summary>
        /// <param name="width">The width</param>
        /// <param name="heigth">The height</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        bool SetPlatformSize(int width, int heigth);

        /// <summary>
        /// Set landing platform starting point
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        bool SetPlatformStartingPoint(int x, int y);

        /// <summary>
        /// Set landing area width and height
        /// </summary>
        /// <param name="width">The width</param>
        /// <param name="heigth">The height</param>
        /// <returns>True if OK, false if incorrect parameters</returns>
        bool SetLandingAreaSize(int width, int heigth);        
    }
}
