using Microsoft.VisualStudio.TestTools.UnitTesting;
using RocketLibrary.Service;
using RocketLibrary.Model;

namespace RocketLibrary.Tests
{
    [TestClass]
    public class LandingTest
    {  
        [TestMethod]
        public void TestOutOfPlatformLanding()
        {
            LandingService LandingService = new LandingService();

            var result = LandingService.CheckLanding(20,20);
            Assert.AreEqual(LandingResult.OutOfPlatform, result);
        }

        [TestMethod]
        public void TestClashLanding()
        {
            LandingService LandingService = new LandingService();

            var result = LandingService.CheckLanding(20, 20);
            Assert.AreEqual(LandingResult.OutOfPlatform, result);

            //Check we get a clash if sending the same position or +1 / -1 coordinates
            result = LandingService.CheckLanding(20, 20);
            Assert.AreEqual(LandingResult.Clash, result);

            result = LandingService.CheckLanding(20, 21);
            Assert.AreEqual(LandingResult.Clash, result);

            result = LandingService.CheckLanding(21, 20);
            Assert.AreEqual(LandingResult.Clash, result);

            result = LandingService.CheckLanding(20, 19);
            Assert.AreEqual(LandingResult.Clash, result);

            //Check we don't get a clash when sending a -2 variation in the y coordinate
            result = LandingService.CheckLanding(20, 21);
            Assert.AreEqual(LandingResult.OutOfPlatform, result);
        }

        [TestMethod]
        public void TestOkLanding()
        {
            LandingService LandingService = new LandingService();

            var result = LandingService.CheckLanding(10, 10);
            Assert.AreEqual(LandingResult.OkForLanding, result);
        }

        [TestMethod]
        public void TestResizePlatform()
        {
            LandingService LandingService = new LandingService();

            //Check the landing is ok in the initial platform
            var result = LandingService.CheckLanding(10, 10);
            Assert.AreEqual(LandingResult.OkForLanding, result);

            //Check the landing is not ok outside the initial platform
            result = LandingService.CheckLanding(20, 20);
            Assert.AreEqual(LandingResult.OutOfPlatform, result);

            //Resize the platform
            LandingService.SetPlatformSize(30, 30);

            //Check the landing is ok with the new platform size
            result = LandingService.CheckLanding(25, 25);
            Assert.AreEqual(LandingResult.OkForLanding, result);
        }

        [TestMethod]
        public void TestMovePlatformStartingPoint()
        {
            LandingService LandingService = new LandingService();

            //Check the landing is ok in the initial platform
            var result = LandingService.CheckLanding(10, 10);
            Assert.AreEqual(LandingResult.OkForLanding, result);            

            //Move the platform
            LandingService.SetPlatformStartingPoint(30, 30);

            //Check the landing is not ok in an area that would be ok for the initial platform position
            result = LandingService.CheckLanding(25, 25);
            Assert.AreEqual(LandingResult.OutOfPlatform, result);

            //Check the landing is ok for a position inside the new platform area
            result = LandingService.CheckLanding(35, 35);
            Assert.AreEqual(LandingResult.OkForLanding, result);
        }

        [TestMethod]
        public void TestResizeLandingArea()
        {
            LandingService LandingService = new LandingService();

            //We can't resize the area to be smaller than the platform
            var result = LandingService.SetLandingAreaSize(5, 5);
            Assert.AreEqual(false, result);

            result = LandingService.SetLandingAreaSize(200, 200);
            Assert.AreEqual(true, result);

            //Check that we can move the platform to a starting point only available after the area resize
            result = LandingService.SetPlatformStartingPoint(170, 170);
            Assert.AreEqual(true, result);
        }
    }
}
